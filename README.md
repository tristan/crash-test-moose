# Crash Test Moose

This is the code behind the website [crashtestmoose.com](https://crashtestmoose.com)

It's mostly pieced together from Giphy embeds and a script or 2 from Stack Overflow.

Have more moose to add? Email [addamoose@crashtestmoose.com](mailto:addamoose@crashtestmoose.com)